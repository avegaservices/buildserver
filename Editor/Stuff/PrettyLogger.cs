﻿using System;
using System.Reflection;
using UnityEngine;

public class PrettyLogger
{
    public enum LogLevel
    {
        Success,
        Info,
        Warning,
        Error
    }

    private static void Log(string text, LogLevel logLevel = LogLevel.Info, string callerClass = null)
    {
        if (callerClass == null)
        {
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
            System.Diagnostics.StackFrame frame = stackTrace.GetFrames()[1];
            MethodBase method = frame.GetMethod();
            callerClass = method.DeclaringType.Name;
        }

        switch (logLevel)
        {
            case LogLevel.Success:
                Debug.LogFormat("<color=#22CC22ff><b>[OK]</b></color> <b>[{0}]</b> {1}", callerClass, text);
                break;
            case LogLevel.Info:
                Debug.LogFormat("<b>[Info]</b> <b>[{0}]</b> {1}", callerClass, text);
                break;
            case LogLevel.Warning:
                Debug.LogWarningFormat("<color=#FFD000FF><b>[Warning]</b></color> <b>[{0}]</b> {1}", callerClass, text);
                break;
            case LogLevel.Error:
                Debug.LogErrorFormat("<color=red><b>[FAIL]</b></color> <b>[{0}]</b> {1}", callerClass, text);
                break;
        }

        if (!Application.isBatchMode) return;
        switch (logLevel)
        {
            case LogLevel.Success:
                Console.WriteLine("[OK][{0}] {1}", callerClass, text);
                break;
            case LogLevel.Info:
                Console.WriteLine("[Info][{0}] {1}", callerClass, text);
                break;
            case LogLevel.Warning:
                Console.WriteLine("[Warning][{0}] {1}", callerClass, text);
                break;
            case LogLevel.Error:
                Console.WriteLine("[FAIL][{0}] {1}", callerClass, text);
                break;
        }
    }

    public static void LogFormat(string format, params object[] args)
    {
        System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
        System.Diagnostics.StackFrame frame = stackTrace.GetFrames()[1];
        MethodBase method = frame.GetMethod();
        string callerClass = method.DeclaringType.Name;

        Log(string.Format(format, args), LogLevel.Info, callerClass);
    }

    public static void LogFormat(string format, LogLevel logLevel, params object[] args)
    {
        System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
        System.Diagnostics.StackFrame frame = stackTrace.GetFrames()[1];
        MethodBase method = frame.GetMethod();
        string callerClass = method.DeclaringType.Name;

        Log(string.Format(format, args), logLevel, callerClass);
    }

    public static void LogFormat(string format, string callerClass, LogLevel logLevel, params object[] args)
    {
        Log(string.Format(format, args), logLevel, callerClass);
    }
}
