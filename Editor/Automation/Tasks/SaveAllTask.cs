﻿using System.Collections;
using UnityEditor;

public class SaveAllTask : AutomatorTaskBase
{
    public override string[] ConfigParameters { get; } = new string[0];
    
    public override IEnumerator Prepare() 
    { 
        AssetDatabase.SaveAssets();
        yield return null; 
    }

    public override IEnumerator Configure(Hashtable data)
    {
        yield return Prepare();
    }
}
