﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

public class ApplyBuildSettingsTask : AutomatorTaskBase
{
	const string packagesDir = 
	#if UNITY_2018_1_OR_NEWER
		"Packages"
	#else
		"UnityPackageManager"
	#endif
	;
	
	enum ConfigKeys
	{
		//AppName,
		//Company,
		//Bundle,
		Version,
		VersionCode,
		KeyStoreName,
		KeyStorePass,
		AliasName,
		AliasPass
	};

	public override string[] ConfigParameters
	{
		get { return Enum.GetNames(typeof(ConfigKeys)); }
	}

	public override IEnumerator Prepare()
	{
		yield return 0;
	}

	public override IEnumerator Configure(Hashtable data)
	{
		Debug.Log("===== Setup BuildSettings =====");
		//PlayerSettings.productName = GetParameter<string>(data, ConfigKeys.AppName) ?? "";
		//PlayerSettings.companyName = GetParameter<string>(data, ConfigKeys.Company) ?? "";
		//PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android,  GetParameter<string>(data, ConfigKeys.Bundle) ?? "");
		//PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS,  GetParameter<string>(data, ConfigKeys.Bundle) ?? "");

		var versionStr = GetParameter<string>(data, ConfigKeys.Version) ?? "";
		if(!string.IsNullOrWhiteSpace(versionStr))
			PlayerSettings.bundleVersion = versionStr;
		
		var versionCode = int.Parse(GetParameter<string>(data, ConfigKeys.VersionCode) ?? "0");
        if (versionCode > 0)
        {
#if UNITY_ANDROID
            PlayerSettings.Android.bundleVersionCode = versionCode;
#else
            PlayerSettings.iOS.buildNumber = versionCode.ToString();
#endif
        }

#if UNITY_ANDROID
#if UNITY_2019_1_OR_NEWER
		PlayerSettings.Android.useCustomKeystore = true;
#endif
		PlayerSettings.Android.keystoreName = GetParameter<string>(data, ConfigKeys.KeyStoreName) ?? "";
		PlayerSettings.Android.keystorePass = GetParameter<string>(data, ConfigKeys.KeyStorePass) ?? "";
		PlayerSettings.Android.keyaliasName = GetParameter<string>(data, ConfigKeys.AliasName) ?? "";
		PlayerSettings.Android.keyaliasPass = GetParameter<string>(data, ConfigKeys.AliasPass) ?? "";
#endif
        yield return 0;
		
		if (Application.HasProLicense())
		{
			Debug.Log("Setting up splash...");

            PlayerSettings.SplashScreen.show =
                PlayerSettings.SplashScreen.background || PlayerSettings.SplashScreen.logos.Length > 0;
			PlayerSettings.SplashScreen.showUnityLogo = false;
			yield return 0;
		}
		
		Debug.Log("Applied Settings:\r\n" +
		          "productName = " + PlayerSettings.productName + "\r\n" +
		          "companyName = " + PlayerSettings.companyName + "\r\n" +
		          "bundleIdentifier = " + PlayerSettings.applicationIdentifier + "\r\n" +
		          "bundleVersion = " + PlayerSettings.bundleVersion + "\r\n" +
#if UNITY_ANDROID
		          "bundleVersionCode = " + PlayerSettings.Android.bundleVersionCode + "\r\n" +
		          "keystoreName = " + PlayerSettings.Android.keystoreName + "\r\n" +
		          "keystorePass = ####" + /*PlayerSettings.Android.keystorePass +*/ "\r\n" +
		          "keyaliasName = " + PlayerSettings.Android.keyaliasName + "\r\n" +
		          "keyaliasPass = ####" /*+ PlayerSettings.Android.keyaliasPass*/
#else
                  "buildNumber = " + PlayerSettings.iOS.buildNumber + "\r\n"                
#endif
                  );
		
		yield return 0;

		AssetDatabase.Refresh();
		
		Debug.Log("===== End Setup BuildSettings =====");
		yield return 0;
	}
}