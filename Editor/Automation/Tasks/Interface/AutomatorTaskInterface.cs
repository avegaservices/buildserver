﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAutomatorTask
{
    IEnumerator Prepare();
    IEnumerator Configure(Hashtable data);
    string GenerateEmptyConfig();
    string CustomParameter {get; set;}
    string CustomConfigName {get;}
}