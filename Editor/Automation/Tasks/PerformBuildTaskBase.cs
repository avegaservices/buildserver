﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public abstract class PerformBuildTaskBase : AutomatorTaskBase {

	enum ConfigKeys
	{
		CustomDefines,
		OutputName,
		OutDir,
	};

	public override string[] ConfigParameters
	{
		get { return Enum.GetNames(typeof(ConfigKeys)); }
	}

	const string CONFIG_FAKE_NAME = "PerformBuildTask";
	public override string CustomConfigName => CONFIG_FAKE_NAME;

	public override IEnumerator Prepare() { yield break; }

	protected virtual BuildTarget Target { get; }
	protected virtual BuildTargetGroup TargetGroup { get; }

	protected abstract string GetVersionCode();
	protected abstract string GetBuildPath(string buildDir, string buildType);

	public override IEnumerator Configure(Hashtable data)
	{
		var customDefines = (GetParameter<string>(data, ConfigKeys.CustomDefines) ?? "")
			.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
			.Select(x => x.Trim())
			.ToArray();
		var cdNames = customDefines.Select(x => x.Replace("!", "")).ToArray();

		var currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(TargetGroup)
			.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
			.Select(x => x.Trim())
			.Where(x => !cdNames.Contains(x.Replace("!", "")))
			.Concat(customDefines)
			.Aggregate((x, y) => $"{x};{y}");
		
		PlayerSettings.SetScriptingDefineSymbolsForGroup(TargetGroup, currentDefines);
		
		var bType = CustomParameter;
		if (string.IsNullOrWhiteSpace(bType))
			bType = "test";
		
		ConfigureBuildType(bType);

		yield return 0;

		var seconds = 3;
		while (seconds > 0)
		{
			Debug.LogFormat("Starting build in: {0}",seconds--);
			yield return new WaitForSecondsRealtime(1);
		}

		if (PerformBuild(GetParameter<string>(data, ConfigKeys.OutDir),
			    GetParameter<string>(data, ConfigKeys.OutputName), bType))
		{
			yield return 0;
		}
		else
		{
			yield return Automator.FAILURE;
		}
	}

	protected abstract void ConfigureBuildType(string buildType);
	
	bool PerformBuild(string outputDir, string outputName, string buildType)
    {
        Debug.Log("=========================\r\n===== BUILD STARTED =====\r\n=========================");

        var buildPath = outputName ?? $"{PlayerSettings.applicationIdentifier}_v{PlayerSettings.bundleVersion}_{GetVersionCode()}";

        if (string.IsNullOrWhiteSpace(outputDir))
	        outputDir = $"{Directory.GetParent(Application.dataPath)}/Builds";
	        
        var dir = $"{outputDir}/{buildPath}";
        if (!Directory.Exists(dir))
	        Directory.CreateDirectory(dir);

        var path = GetBuildPath(dir, buildType);
        
        Debug.Log("Start build project. BuildPath: " + path);

        if (string.IsNullOrEmpty(path) || !MakeBuild(dir, path, FindEnabledEditorScenes()))
        {
	        Debug.LogError("===== Building Failed! =====");
	        return false;
        }
        
	    Debug.Log("===== Building Successfull! =====");
	    return true;
    }

	protected virtual bool MakeBuild(string outputDir, string outputPath, params string[] scenes) =>
		BuildPipeline.BuildPlayer(new BuildPlayerOptions
			{
				scenes = scenes,
				target = Target,
				options = BuildOptions.None,
				locationPathName = outputPath
			})
			.summary
			.result == BuildResult.Succeeded;

	/// <summary>
	/// Массив сцен для включения в билд. По-умолчанию берутся все, что включены в настройках билда в юнити
	/// </summary>
	string[] FindEnabledEditorScenes()
	{
		List<string> EditorScenes = new List<string>();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
		{
			if (!scene.enabled) continue;
			EditorScenes.Add(scene.path);
		}
		return EditorScenes.ToArray();
	}
}
