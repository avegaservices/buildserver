﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class OpenSceneTask : AutomatorTaskBase {
	public override string[] ConfigParameters
	{
		get { return new string[0];}
	}

	public override IEnumerator Prepare()
	{
		var parameter = CustomParameter;
		if (string.IsNullOrEmpty(parameter))
		{
			Debug.LogWarning("CustomParameter is empty, default value is \"0\"");
			parameter = "0";
		}
		
		var buildScenes = EditorBuildSettings.scenes
			.Where(x => x.enabled)
			.Select(x => x.path)
			.ToList();
		
		var path = "";
		
		int sceneIdx;
		if (int.TryParse(parameter, out sceneIdx))
		{
			if (sceneIdx < 0 || sceneIdx >= buildScenes.Count)
				throw new Exception("Scene index out of bounds");

			path = buildScenes[sceneIdx];
		}
		else if (Regex.IsMatch(parameter, @"[\\\/]"))
		{
			path = parameter.Trim();
			if (!path.EndsWith(".unity")) path += ".unity";
		}
		else
		{
			path = buildScenes.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == parameter);
			if(string.IsNullOrEmpty(path))
				throw new Exception("Scene named \""+parameter+"\" not found in build settings");
		}
		
		if (!EditorSceneManager.OpenScene(path).IsValid())
			throw new Exception("Can't find scene \""+path+"\"");

		PrettyLogger.LogFormat("Scene {0} opened", GetType().Name, PrettyLogger.LogLevel.Info,
			Path.GetFileNameWithoutExtension(path));
		
		yield return 0;
	}

	public override IEnumerator Configure(Hashtable data)
	{
		yield return Prepare();
	}
}
