﻿using System.Collections;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class SaveSceneTask : AutomatorTaskBase {
	public override string[] ConfigParameters
	{
		get { return new string[0];}
	}

	public override IEnumerator Prepare()
	{
		EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
		PrettyLogger.LogFormat("Scenes saved", GetType().Name, PrettyLogger.LogLevel.Info);
		yield return 0;
	}

	public override IEnumerator Configure(Hashtable data)
	{
		yield return Prepare();
	}
}