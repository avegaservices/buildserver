﻿using UnityEditor;
using UnityEngine;

public class PerformIosBuildTask : PerformBuildTaskBase
{
    protected override BuildTarget Target => BuildTarget.iOS;
    protected override BuildTargetGroup TargetGroup => BuildTargetGroup.iOS;
    
    protected override string GetVersionCode() => PlayerSettings.iOS.buildNumber;

    protected override string GetBuildPath(string buildDir, string buildType) => buildDir;

    protected override void ConfigureBuildType(string buildType)
    {
        PlayerSettings.SetArchitecture(BuildTargetGroup.iOS, 1); //arm64
        
        switch (buildType)
        {
            case "release":
                PlayerSettings.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);
                PlayerSettings.SetStackTraceLogType(LogType.Assert, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Warning, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Exception, StackTraceLogType.ScriptOnly);

                break;
        }
    }
}
