using System;
using System.Collections;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

public abstract class AutomatorTaskBase : IAutomatorTask
{
    public abstract string[] ConfigParameters { get; }
    public virtual string CustomConfigName => null;

    public abstract IEnumerator Configure(Hashtable data);
    public abstract IEnumerator Prepare();

    /// <summary>
    /// Gets the parameter. Returns null if parameter doesn't exist
    /// </summary>
    /// <returns>The parameter value or null.</returns>
    /// <param name="data">Data.</param>
    /// <param name="paramName">Parameter name.</param>
    protected T GetParameter<T>(Hashtable data, string paramName) where T : class
    {
        if (data.ContainsKey(paramName))
            return (T) data[paramName];
        else
        {
            PrettyLogger.LogFormat("Parameter {0} doesn't exist in the config. Returning default value.",
                GetType().Name, PrettyLogger.LogLevel.Warning, paramName);
            return default(T);
        }
    }
    
    /// <summary>
    /// Gets the parameter. Returns null if parameter doesn't exist
    /// </summary>
    /// <returns>The parameter value or null.</returns>
    /// <param name="data">Data.</param>
    /// <param name="paramName">Parameter name enum.</param>
    protected T GetParameter<T>(Hashtable data, Enum paramName) where T : class
    {
        return GetParameter<T>(data, paramName.ToString());
    } 

    /// <summary>
    /// Generates the empty config.
    /// </summary>
    /// <returns>The empty config.</returns>
    [CanBeNull]
    public string GenerateEmptyConfig()
    {
        var config = ConfigParameters;
        if (config.Length < 1) return null;
        
        var sb = new StringBuilder("{\n");

        string EmptyParameter(string pName) => $"\"{pName}\": \"\"";

        sb.Append($"  {EmptyParameter(config.First())}");
        
        foreach (var c in ConfigParameters.Skip(1)) 
            sb.Append($",\n  {EmptyParameter(c)}");

        sb.Append("\n}");

        return sb.ToString();
    }
    
    string IAutomatorTask.CustomParameter{get;set;}
    protected string CustomParameter
    {
        get { return ((IAutomatorTask) this).CustomParameter; }
    }
}