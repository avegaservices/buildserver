﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PerformAndroidBuildTask : PerformBuildTaskBase
{
    protected override BuildTarget Target => BuildTarget.Android;
    protected override BuildTargetGroup TargetGroup => BuildTargetGroup.Android;

    protected override string GetVersionCode() => PlayerSettings.Android.bundleVersionCode.ToString();

    protected override string GetBuildPath(string buildDir, string buildType)
    {
        string fn = $"{PlayerSettings.applicationIdentifier}_{PlayerSettings.bundleVersion}_{GetVersionCode()}_{buildType}";
        return $"{buildDir}/{fn}.apk";
    }

    protected virtual bool IsAABBuild() => false;

    protected override void ConfigureBuildType(string buildType)
    {
        switch (buildType)
		{
			case "test":
                PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARM64;
                
#if UNITY_2020_1_OR_NEWER
                PlayerSettings.Android.minifyRelease = false;
                PlayerSettings.Android.minifyWithR8 = false;
#else
                EditorUserBuildSettings.androidReleaseMinification = AndroidMinification.None;
#endif

				
				EditorUserBuildSettings.androidCreateSymbolsZip = false;
                break;
			case "release":
                PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

#if UNITY_2020_1_OR_NEWER
                PlayerSettings.Android.minifyRelease = true;
                PlayerSettings.Android.minifyWithR8 = true;
#else
                EditorUserBuildSettings.androidReleaseMinification = AndroidMinification.Proguard;
#endif
				EditorUserBuildSettings.androidCreateSymbolsZip = true;

                PlayerSettings.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);
				PlayerSettings.SetStackTraceLogType(LogType.Assert, StackTraceLogType.None);
				PlayerSettings.SetStackTraceLogType(LogType.Warning, StackTraceLogType.None);
				PlayerSettings.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
				PlayerSettings.SetStackTraceLogType(LogType.Exception, StackTraceLogType.ScriptOnly);

                break;
		}

		EditorUserBuildSettings.buildAppBundle = IsAABBuild();
    }

    protected override bool MakeBuild(string outputDir, string outputPath, params string[] scenes)
    {
	    var res = base.MakeBuild(outputDir, outputPath, scenes);
	    if (res)
	    {
		    if (PlayerSettings.Android.useAPKExpansionFiles)
		    {
			    var obbn = Directory.GetFiles(outputDir, $"{PlayerSettings.applicationIdentifier}*.obb").FirstOrDefault();
			    if (obbn != default)
				    File.Move(obbn,
					    $"{outputDir}/main.{PlayerSettings.Android.bundleVersionCode}.{PlayerSettings.applicationIdentifier}.obb");
		    }
	    }

	    return res;
    }
}
