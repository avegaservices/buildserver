﻿using System.Collections;
using Unity.EditorCoroutines.Editor;
using UnityEngine;

public class PauseTask : AutomatorTaskBase
{
    public override string[] ConfigParameters { get; } = new string[0];

    public override IEnumerator Prepare()
    {
        if (!float.TryParse(CustomParameter, out var pauseTime))
        {
            Debug.LogWarning($"[{nameof(PauseTask)}] failed to parse pause time, skipping...");

            yield return null;
            yield break;
        }
        
        Debug.Log($"[{nameof(PauseTask)}] waiting {pauseTime} seconds...");
        yield return new EditorWaitForSeconds(pauseTime);
    }

    public override IEnumerator Configure(Hashtable data)
    {
        yield return Prepare();
    }
}
