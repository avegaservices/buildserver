﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class TaskSelector
{
	public static TaskSelector Current
	{
		get
		{
			if (_current == null)
			{
				_current = new TaskSelector();
				_current.RescanTasks(EditorUserBuildSettings.selectedBuildTargetGroup, Automator.BuildType);
			}
			return _current;
		}
	}

	static TaskSelector _current;

	[Serializable]
	public class AutomationTask
	{
		public string classFullName;
		public bool enabled;
		public string paramString;

		public AutomationTask(){}
		
		public AutomationTask(string classFullName)
		{
			this.classFullName = classFullName;
		}
		
		public string DisplayName
		{
			get
			{
				if (string.IsNullOrEmpty(classFullName)) return classFullName;
				return Regex.Match(classFullName, @"(?>.+\.)?(.+)").Groups[1].Value;
			}
		}
	}

	public List<AutomationTask> allTasks,availableTasks;
	public TasksConfig currentConfig;
	
	[Serializable]
	public class TasksConfig
	{
		private string _path;
		public List<SerializedTask> tasks = new List<SerializedTask>();
		
		[Serializable]
		public class SerializedTask
		{
			public string task;
			public bool enabled;
			public string paramString;
		}

		public static TasksConfig LoadFromConfig(string path)
		{
			path += "/TaskSelectorConfig.json";
			var config = File.Exists(path)
				? JsonUtility.FromJson<TasksConfig>(File.ReadAllText(path))
				: new TasksConfig();
			config._path = path;
			return config;
		}

		public void SaveToConfig()
		{
			var dirName = Path.GetDirectoryName(_path);
			if (!Directory.Exists(dirName))
				Directory.CreateDirectory(dirName);
			File.WriteAllText(_path,JsonUtility.ToJson(this,true));
			AssetDatabase.Refresh();
		}
	}
	
	public void RescanTasks(BuildTargetGroup platform, string buildType)
	{
		availableTasks = AppDomain.CurrentDomain.GetAssemblies()
			.SelectMany(x => x.GetTypes())
			.Where(x => typeof(IAutomatorTask).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
			.Select(x => new AutomationTask(x.FullName))
			.ToList();

		currentConfig = TasksConfig.LoadFromConfig(Automator.GetConfigsPath(platform, buildType));
		
		allTasks = currentConfig.tasks
			.Join(availableTasks, x => x.task, x => x.DisplayName, (cfg, tsk) => new AutomationTask(tsk.classFullName)
			{
				enabled = cfg.enabled,
				paramString = cfg.paramString
			})
			.Concat(availableTasks
				.Where(x=>currentConfig.tasks.All(y => y.task != x.DisplayName))
				.Select(tsk=>new AutomationTask(tsk.classFullName)))
			.ToList();
	}

	public void ApplyConfig()
	{
		if(currentConfig == null)
			return;

		currentConfig.tasks = allTasks
			.Select(x => new TasksConfig.SerializedTask
			{
				task = x.DisplayName, 
				enabled = x.enabled,
				paramString = x.paramString
			})
			.ToList();
		
		currentConfig.SaveToConfig();
	}

	public List<KeyValuePair<string,string>> EnabledTasks
	{
		get
		{
			return allTasks.Where(x => x.enabled)
				.Select(x => new KeyValuePair<string,string>(x.classFullName,x.paramString))
				.ToList();
		}
	}
	
	
}