﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Unity.EditorCoroutines.Editor;
using Exception = System.Exception;

public class Automator
{
    public const string CONFIGS_PATH_BASE = "Assets/Editor/Automation/Configs";

    public const string FAILURE = "Failure"; 
    
    public static string BuildType => Environment.GetEnvironmentVariable("BUILD_TYPE", EnvironmentVariableTarget.Process);

    public static string GetConfigsPath(BuildTargetGroup platform, string buildtype) =>
        $"{CONFIGS_PATH_BASE}/{platform:G}/{buildtype}";
    public static string ConfigsPath => GetConfigsPath(EditorUserBuildSettings.selectedBuildTargetGroup, BuildType);

    public static Automator Instance
    {
        get
        {
            if (instance == null)
                instance = new Automator();
            return instance;
        }
    }

    static Automator instance;

    List<KeyValuePair<string,IAutomatorTask>> tasks = new List<KeyValuePair<string, IAutomatorTask>>();

    /// <summary>
    /// Finds tasks in the task directory.
    /// </summary>
    void FindTasks()
    {
        var eTasks = TaskSelector.Current.EnabledTasks;
        
        tasks.Clear();
        
        foreach (var taskAndParam in eTasks)
        {
            string className = taskAndParam.Key;

            try
            {
                var inst = (IAutomatorTask) AppDomain.CurrentDomain.GetAssemblies()
                    .Select(ass => ass.CreateInstance(className))
                    .FirstOrDefault(x => x != null);
                
                if (inst != null) inst.CustomParameter = taskAndParam.Value;

                tasks.Add(new KeyValuePair<string, IAutomatorTask>(className,inst));
            }
            catch (Exception e)
            {
                Debug.LogFormat(
                    "<color=red><b>FAIL</b></color> <b>[Automator]</b> Error while creating task. Class name: {0}.",
                    className);
                Debug.LogException(e);
            }
        }
    }

    /// <summary>
    /// Loads the config.
    /// </summary>
    /// <returns>The config.</returns>
    /// <param name="taskName">Task name.</param>
    Hashtable LoadConfig(string taskName)
    {
        string configName = taskName.Replace("Task", "Config") + ".json";
        string configPath = Path.Combine(ConfigsPath, configName);
        if (File.Exists(configPath))
            return JsonConvert.DeserializeObject<Hashtable>(File.ReadAllText(configPath));
        
        return new Hashtable();
    }

    void GenerateEmptyConfigForTask(string taskName, bool forceOverwrite)
    {
        var idx = tasks.FindIndex(x => x.Key == taskName);
        
        if (idx>=0)
        {
            string configName = (tasks[idx].Value.CustomConfigName ?? taskName).Replace("Task", "Config") + ".json";
            string configPath = Path.Combine(ConfigsPath, configName);

            if (!File.Exists(configPath))
            {
                File.WriteAllText(configPath, tasks[idx].Value.GenerateEmptyConfig());
                PrettyLogger.LogFormat("Generating config for {0}... Done.", PrettyLogger.LogLevel.Success, taskName);
            }
            else
            {
                if (forceOverwrite ||
                    EditorUtility.DisplayDialog(
                        "Confirm Overwrite",
                        string.Format(
                            "Config for the {0} already exists.\r\nIt can contain some important data like api keys, etc.\r\nDo you want to replace it with the new empty config?",
                            taskName),
                        "Yes",
                        "No"))
                {
                    File.WriteAllText(configPath, tasks[idx].Value.GenerateEmptyConfig());
                    PrettyLogger.LogFormat("Generating config for {0}... File already exists. Overwritten.",
                        PrettyLogger.LogLevel.Success, taskName);
                }
                else
                    PrettyLogger.LogFormat("Generating config for {0}... File already exists. Skip.",
                        PrettyLogger.LogLevel.Warning, taskName);
            }
        }
    }

    void GenerateEmptyConfigs(bool forceOverwrite, params string[] tasksToProcess)
    {
        if (tasksToProcess.Length == 0)
        {
            foreach (var taskItem in tasks)
                GenerateEmptyConfigForTask(taskItem.Key, forceOverwrite);
        }
        else
        {
            foreach (string tasksItem in tasksToProcess)
                GenerateEmptyConfigForTask(tasksItem, forceOverwrite);
        }

        AssetDatabase.Refresh();
    }

    IEnumerator EnumerateTasks(string messageFormat, Func<KeyValuePair<string,IAutomatorTask>, IEnumerator> enumeratorGetter)
    {
        var idx = 0;
        foreach (var taskData in tasks)
        {
            EditorUtility.DisplayProgressBar(string.Format(messageFormat, "task"),
                taskData.Value.GetType().Name+" ("+ ++idx +"/"+tasks.Count+")",
                (float) idx / tasks.Count);

            var enumerator = enumeratorGetter(taskData);

            yield return enumerator;
            if (enumerator.Current is FAILURE)
            {
                PrettyLogger.LogFormat(messageFormat, GetType().Name, PrettyLogger.LogLevel.Error, taskData.Key);
                yield break;
            }

            PrettyLogger.LogFormat(messageFormat, GetType().Name, PrettyLogger.LogLevel.Success, taskData.Key);
            yield return 0;
        }
        yield return 0;
    }

    IEnumerator Prepare()
    {
        return EnumerateTasks("Prepare {0}", x => x.Value.Prepare());
    }

    IEnumerator Configure()
    {
        return EnumerateTasks("Configure {0}", x =>
            x.Value.Configure(LoadConfig(x.Value.CustomConfigName ?? x.Key)));
    }

    IEnumerator AutomateInternal()
    {
        FindTasks();
        
        EditorApplication.LockReloadAssemblies();

        var enumerator = Prepare();
        yield return enumerator;
        var isFailure = enumerator.Current is FAILURE;
        if (!isFailure)
        {
            enumerator = Configure();
            isFailure = enumerator.Current is FAILURE;
            yield return enumerator;
        }
        EditorUtility.ClearProgressBar();
        isRunning = false;

        if (Application.isBatchMode) EditorApplication.Exit(isFailure ? 1 : 0);
        
        EditorApplication.UnlockReloadAssemblies();
    }

    private static bool isRunning = false;

    public static void Automate()
    {
        if (!isRunning)
        {
            isRunning = true;

            EditorCoroutineUtility.StartCoroutineOwnerless(Instance.AutomateInternal());
        }
    }

    public static void GenerateConfigs()
    {
        Instance.FindTasks();
        Instance.GenerateEmptyConfigs(false);
    }
}