﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class TaskSelectorWindow : EditorWindow
{
	private ReorderableList _list;
	
	[MenuItem("Tools/Automator/Task Selector Window")]
	public static void Init()
	{
		var wnd = GetWindow<TaskSelectorWindow>();
		wnd.Show();
	}

	private bool changedFlag;
	void InitInternal()
	{
		changedFlag = false;
		_list = new ReorderableList(TaskSelector.Current.allTasks, typeof(TaskSelector.AutomationTask), true, true, true, true);

		_list.drawHeaderCallback = rect =>
		{
			EditorGUI.LabelField(rect, (changedFlag ? "*not saved* " : "") + "Automation tasks order:");
			
			rect.xMin = rect.xMax - 44;
			rect.width = 20;

			Action<bool> setAll = isOn =>
			{
				TaskSelector.Current.allTasks.ForEach(x=>x.enabled = isOn);
				changedFlag = true;
			};
			
			if (GUI.Button(rect, "☐")) setAll(false);
			
			rect.position+= Vector2.right*22;
			
			if (GUI.Button(rect, "☑")) setAll(true);
		};

		var taskOptions = TaskSelector.Current.availableTasks.Select(x => x.DisplayName).ToArray();
		
		_list.drawElementCallback = (rect, index, active, focused) =>
		{
			var elem = TaskSelector.Current.allTasks[index];

			rect.width /= 3.22f;
			
			using (var checker = new EditorGUI.ChangeCheckScope())
			{
				var idx = taskOptions.ToList().FindIndex(x =>
				{
					return x == elem.DisplayName;
				});
				if (idx < 0) idx = 0;
				elem.classFullName = TaskSelector.Current.availableTasks[EditorGUI.Popup(rect, idx, taskOptions)].classFullName;
				rect.x += rect.width+5;
				elem.enabled = EditorGUI.ToggleLeft(rect, "enable", elem.enabled);
				rect.x += rect.width+5;
				elem.paramString = EditorGUI.TextField(rect, elem.paramString);

				if (checker.changed)
					changedFlag = true;
			}
		};
		_list.onChangedCallback = list =>
			changedFlag = true;
	}

	void InitPlatforms()
	{
		_selectedPlatform = EditorUserBuildSettings.selectedBuildTargetGroup;
		_selectedBuildType = Automator.BuildType;

		if (string.IsNullOrWhiteSpace(_selectedBuildType))
			_selectedBuildType = FindConfigs(_selectedPlatform.Value).FirstOrDefault();
	}

	private Vector2 _scroll;
	private BuildTargetGroup? _selectedPlatform;
	private string _selectedBuildType;
	private Page _currentPage = Page.Main;

	string[] FindConfigs(BuildTargetGroup platform)
	{
		try
		{
			return Directory.GetDirectories($"{Automator.CONFIGS_PATH_BASE}/{platform:G}")
				.Select(Path.GetFileName)
				.ToArray();
		}
		catch
		{
			return new string[0];
		}
	}

	private void OnGUI()
	{
		if(_list == null) InitInternal();
		if(!_selectedPlatform.HasValue) InitPlatforms();

		switch (_currentPage)
		{
			case Page.Main:
				DrawMainPage();
				break;
			case Page.AddConfig:
				DrawAddConfigPage();
				break;
		}
	}

	void DrawMainPage()
	{
		using (new GUILayout.HorizontalScope())
		{
			EditorGUILayout.LabelField("Automation shortcuts:",GUILayout.MaxWidth(130));

			if (GUILayout.Button("Automate"))
				Automator.Automate();
			if (GUILayout.Button("Generate Configs"))
				Automator.GenerateConfigs();
		}

		using (new GUILayout.HorizontalScope())
		{
			using (var scope = new EditorGUI.ChangeCheckScope())
			{
				_selectedPlatform = (BuildTargetGroup)EditorGUILayout.EnumPopup("Platform", _selectedPlatform);
				var options = FindConfigs(_selectedPlatform.Value);
				if (options.Length > 0)
				{
					var idx = options.ToList().FindIndex(x => x == _selectedBuildType);
					idx = EditorGUILayout.Popup(idx, options);

					if (idx >= 0) 
						_selectedBuildType = options[idx];
				}

				if (scope.changed)
				{
					Environment.SetEnvironmentVariable("BUILD_TYPE", _selectedBuildType, EnvironmentVariableTarget.Process);
					TaskSelector.Current.RescanTasks(_selectedPlatform.Value, _selectedBuildType);
					InitInternal();
					return;
				}
			}

			if (GUILayout.Button("Add"))
			{
				_currentPage = Page.AddConfig;
				return;
			}
		}

		GUILayout.Space(EditorGUIUtility.singleLineHeight * 1.3f);

		using (var scope = new EditorGUILayout.ScrollViewScope(_scroll,GUIStyle.none,GUI.skin.verticalScrollbar))
		{
			_scroll = scope.scrollPosition;

			GUI.color = changedFlag ? new Color(1f, 0.99f, 0.8f) : Color.white;
			_list.DoLayoutList();
		}
		
		EditorGUILayout.Space();
		using (new GUILayout.HorizontalScope())
		{
			GUI.color = changedFlag ? Color.red : Color.white;
			if (GUILayout.Button("Load from json"))
			{
				TaskSelector.Current.RescanTasks(_selectedPlatform.Value, _selectedBuildType);
				InitInternal();
			}
			GUI.color = changedFlag ? Color.yellow : Color.white;
			if (GUILayout.Button("Save to json"))
			{
				TaskSelector.Current.ApplyConfig();
				changedFlag = false;
			}
		}
	}


	private string _newBtName;
	void DrawAddConfigPage()
	{
		if (GUILayout.Button("<"))
		{
			_currentPage = Page.Main;
			return;
		}
		_newBtName = EditorGUILayout.TextField("Add build type", _newBtName);
		if (GUILayout.Button("Add"))
		{
			Directory.CreateDirectory($"{Automator.CONFIGS_PATH_BASE}/{_selectedPlatform:G}/{_newBtName}");
			_currentPage = Page.Main;
			AssetDatabase.Refresh();
		}
	}

	enum Page
	{
		Main,
		AddConfig
	}
}